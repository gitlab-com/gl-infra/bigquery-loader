package main

import "gitlab.com/gitlab-com/gl-infra/bigloader/cmd"

func main() {
	cmd.Execute()
}
