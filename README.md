# BigQuery Job Loader Utility

A basic tool for loading log data into BigQuery.

## Operating the `bigquery-loader`

1. This tool is specifically designed to load GitLab.com log data from Google Cloud Storage, where it is stored in Newline Delimited JSON (NDJSON) format into BigQuery tables.
1. Due to idiosyncracies in our data, it's not possible to load this data directly using the BigQuery loader tools.
1. This loader will "massage" the data before loading it into BigQuery.
1. The loader loads one day at a time. If it detects that a day's worth of data has already been loaded, it will skip that date. **It is recommended not to load today's data**.
1. Definition files are stored in the [`definitions`](definitions/) directory. One definition exists for each log type. This definition is used by the bigquery loader to load data from GCS, perform transformations on the data and partition in in the final destination table.

## Loading a Single Days Data

Use the `load-day` subcommand:

```console
$ # load data for 2022-03-14 from the Sidekiq logs
$ bigloader load-day --dataset foo -d "2022-03-14" definitions/sidekiq.jsonnet
...
```

## Loading a Range of Data

Use the `load-range` subcommand:

```console
$ # load 30 days worth of data, until 2022-03-14, from the Sidekiq logs
$ bigloader load-range --dataset foo --until "2022-03-14" --days 30 definitions/sidekiq.jsonnet
...
```

## Caveats

Very much a work in progress.
