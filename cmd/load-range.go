package cmd

import (
	"log"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/gitlab-com/gl-infra/bigloader/internal/load"
)

var untilDate string
var numberOfDays int

var loadRangeCmd = &cobra.Command{
	Use:   "load-range",
	Short: "Load log data range into bigquery",
	Long:  `Runs a range job load`,
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		layout := "2006-01-02"
		until, err := time.Parse(layout, untilDate)
		if err != nil {
			return err
		}

		return load.LoadLogDataForRange(args[0], until, numberOfDays, datasetId)
	},
}

func init() {
	rootCmd.AddCommand(loadRangeCmd)

	loadRangeCmd.PersistentFlags().StringVarP(&untilDate, "until", "u", "", "Date to load until")
	loadRangeCmd.PersistentFlags().IntVarP(&numberOfDays, "days", "d", 1, "Number of days to load")
	loadRangeCmd.PersistentFlags().StringVar(&datasetId, "dataset", "", "Dataset to load into")

	err := loadRangeCmd.MarkPersistentFlagRequired("dataset")
	if err != nil {
		log.Fatal(err)
	}
}
