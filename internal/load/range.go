package load

import (
	"time"
)

func LoadLogDataForRange(definitionFile string, loadDate time.Time, numberOfDays int, datasetId string) error {
	for i := 0; i < numberOfDays; i++ {
		err := LoadLogDataForDate(definitionFile, loadDate.AddDate(0, 0, -i), datasetId)
		if err != nil {
			return err
		}
	}
	return nil
}
