{
  id: 'registry',
  location: 'gs://gitlab-gprd-logging-archive/gke/registry/dt=%(year)04d-%(month)02d-%(day)02d*',
  partitioningField: 'time',
  fields:
    [
      {
        name: 'time',
        type: 'TIMESTAMP',
        mode: 'REQUIRED',
        extractScalar: '$.time',
        parseTimestamp: '%FT%H:%M:%E*SZ',
      },
      {
        name: 'auth_user_name',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.auth_user_name',
      },
      {
        name: 'root_repo',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.root_repo',
      },
      {
        name: 'vars_name',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.vars_name',
      },
      {
        name: 'digest',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.digest',
      },
      {
        name: 'remote_ip',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.remote_ip',
      },
      {
        name: 'correlation_id',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.correlation_id',
      },
      {
        name: 'msg',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.msg',
      },
      {
        name: 'size_bytes',
        type: 'INT64',
        mode: 'NULLABLE',
        extractScalar: '$.size_bytes',
        safeCast: 'INT64',
      },
    ],
}
