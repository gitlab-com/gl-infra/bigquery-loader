local projectId = 'gitlab-production';

// Remove our own components from the field definition
local fieldsToSchema(fields) =
  std.map(
    function(f)
      {
        name: f.name,
        type: f.type,
        mode: f.mode,
      },
    fields
  );

local generateFieldTransformTerm(f, includeAs=true) =
  local parseTimestamp = std.get(f, 'parseTimestamp', null);
  local safeCast = std.get(f, 'safeCast', null);

  local p1 = 'JSON_EXTRACT_SCALAR(json, "%s")' % [f.extractScalar];
  local p2 = if parseTimestamp != null then
    'PARSE_TIMESTAMP("%s", %s)' % [parseTimestamp, p1]
  else
    p1;

  local p3 = if safeCast != null then
    'SAFE_CAST(%s as %s)' % [p2, safeCast]
  else
    p2;

  if includeAs then
    '%s as %s' % [p3, f.name]
  else
    p3;

local generatePartitioningFieldFilter(definition) =
  local partitioningFieldDefinition = std.filter(function(f) f.name == definition.partitioningField, definition.fields)[0];
  local term = generateFieldTransformTerm(partitioningFieldDefinition, includeAs=false);
  '%s IS NOT NULL' % [term];

local generateTransformQuery(definition, datasetId, preloadTableId) =
  local fieldExtracts = std.map(function(f) generateFieldTransformTerm(f), definition.fields);
  local partitioningFieldFilter = generatePartitioningFieldFilter(definition);

  std.lines([
    'SELECT',
    std.join(',\n', fieldExtracts),
    'FROM `%s.%s.%s`' % [projectId, datasetId, preloadTableId],
    'WHERE',
    partitioningFieldFilter,
  ]);

function(definition, dateArray, datasetId)
  local year = dateArray[0];
  local month = dateArray[1];
  local day = dateArray[2];

  local formatConfig = definition {
    year: year,
    month: month,
    day: day,
  };

  local preloadTableId = 'preload_%(id)s_logs_%(year)04d_%(month)02d_%(day)02d' % formatConfig;

  {
    projectId: projectId,
    datasetId: datasetId,
    tableId: '%(id)s_logs' % definition,
    preloadTableId: preloadTableId,
    partitioningField: definition.partitioningField,
    loadLocation: definition.location % {
      year: year,
      month: month,
      day: day,
    },
    schema: std.manifestJson(fieldsToSchema(definition.fields)),
    transformQuery: generateTransformQuery(definition, datasetId, preloadTableId),
  }
