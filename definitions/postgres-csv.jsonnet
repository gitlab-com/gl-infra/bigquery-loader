{
  id: 'postgres-csv',
  location: 'gs://gitlab-gprd-logging-archive/postgres.postgres_csv/%(year)04d/%(month)02d/%(day)02d*',
  partitioningField: 'time',
  fields:
    [
      {
        name: 'time',
        type: 'TIMESTAMP',
        mode: 'REQUIRED',
        extractScalar: '$.timestamp',
        parseTimestamp: '%FT%H:%M:%E*SZ',
      },
      {
        name: 'application',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: "$.jsonPayload['application']",
      },
      {
        name: 'command_tag',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.jsonPayload.command_tag',
      },
      {
        name: 'correlation_id',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.jsonPayload.correlation_id'
      },
      {
        name: 'database_name',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.jsonPayload.database_name'
      },
      {
        name: 'db_config_name',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.jsonPayload.db_config_name'
      },
      {
        name: 'duration_s',
        type: 'FLOAT',
        mode: 'NULLABLE',
        extractScalar: '$.jsonPayload.duration_s',
        safeCast: 'FLOAT64',
      },
      {
        name: 'endpoint_id',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.jsonPayload.endpoint_id',
      },
      {
        name: 'fingerprint',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.jsonPayload.fingerprint',
      },
      {
        name: 'sql',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.jsonPayload.sql',
      },
      {
        name: 'sql_state_code',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.jsonPayload.sql_state_code',
      },
      {
        name: 'type',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.jsonPayload.type',
      },
    ],
}
