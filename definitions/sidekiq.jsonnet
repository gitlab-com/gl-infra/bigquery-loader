local standardFields = import 'standard-fields.libsonnet';

{
  id: 'sidekiq',
  // IMPORTANT: we only load data from 14h00 to 15h00 each day as a sample of a huge dataset!
  location: 'gs://gitlab-gprd-logging-archive/gke/sidekiq/dt=%(year)04d-%(month)02d-%(day)02d/%(year)04d%(month)02d%(day)02d14*',
  partitioningField: 'time',
  fields:
    [
      {
        name: 'time',
        type: 'TIMESTAMP',
        mode: 'REQUIRED',
        extractScalar: '$.time',
        parseTimestamp: '%FT%H:%M:%E*SZ',
      },
      {
        name: 'class',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.class',
      },
      {
        name: 'shard',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.shard',
      },
      {
        name: 'cpu_s',
        type: 'FLOAT',
        mode: 'NULLABLE',
        extractScalar: '$.cpu_s',
        safeCast: 'FLOAT64',
      },
      {
        name: 'meta_root_namespace',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: "$['meta.root_namespace']",
      },
      {
        name: 'meta_project',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: "$['meta.project']",
      },
      {
        name: 'duration_s',
        type: 'FLOAT',
        mode: 'NULLABLE',
        extractScalar: '$.duration_s',
        safeCast: 'FLOAT64',
      },
      {
        name: 'external_http_count',
        type: 'INT64',
        mode: 'NULLABLE',
        extractScalar: "$.external_http_count",
        safeCast: 'INT64',
      },
      {
        name: 'external_http_duration_s',
        type: 'FLOAT',
        mode: 'NULLABLE',
        extractScalar: '$.external_http_duration_s',
        safeCast: 'FLOAT64',
      },
      {
        name: 'gitaly_calls',
        type: 'INT64',
        mode: 'NULLABLE',
        extractScalar: "$.gitaly_calls",
        safeCast: 'INT64',
      },
      {
        name: 'gitaly_duration_s',
        type: 'FLOAT',
        mode: 'NULLABLE',
        extractScalar: '$.gitaly_duration_s',
        safeCast: 'FLOAT64',
      },
      {
        name: 'job_status',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: "$.job_status",
      },
    ] + standardFields.postgres()
}
