local registry = import './registry.jsonnet';

registry + {
  location: 'gs://gitlab-gprd-logging-archive/gke/registry/%(year)04d/%(month)02d/%(day)02d/*',
}
