{
  postgres()::
    [
      {
        name: 'db_write_count',
        type: 'INT64',
        mode: 'NULLABLE',
        extractScalar: '$.db_write_count',
        safeCast: 'INT64',
      },
    ] +
    std.flatMap(
      function(dbType)
        [
          {
            name: '%scount' % [dbType],
            type: 'INT64',
            mode: 'NULLABLE',
            extractScalar: '$.%scount' % [dbType],
            safeCast: 'INT64',
          },
          {
            name: '%sduration_s' % [dbType],
            type: 'FLOAT',
            mode: 'NULLABLE',
            extractScalar: '$.%sduration_s' % [dbType],
            safeCast: 'FLOAT64',
          },
        ],
      ['db_', 'db_ci_', 'db_primary_', 'db_main_', 'db_main_replica_']
    ),
}
